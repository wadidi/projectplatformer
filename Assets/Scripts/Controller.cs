using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {


    public float speed = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        float x = Input.GetAxis("X axis");
        float y = Input.GetAxis("Y axis");

        this.transform.position += -y * speed * Time.deltaTime * this.transform.forward;
        this.transform.position += x * speed * Time.deltaTime * this.transform.right;

    }
}
