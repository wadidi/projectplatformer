using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerTest : MonoBehaviour {

    public Color A_btnColor = Color.green;
    public Color B_btnColor = Color.red;
    public Color X_btnColor = Color.blue;
    public Color Y_btnColor = Color.yellow;

    private Vector3 center;

    // Use this for initialization
    void Start () {
        center = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
        // buttons ----------------------------------------------------------
        if (Input.GetButton("A"))
        {
            this.GetComponent<Renderer>().material.color = A_btnColor;
        }
        else if (Input.GetButton("B"))
        {
            this.GetComponent<Renderer>().material.color = B_btnColor;
        }
        else if (Input.GetButton("X"))
        {
            this.GetComponent<Renderer>().material.color = X_btnColor;
        }
        else if (Input.GetButton("Y"))
        {
            this.GetComponent<Renderer>().material.color = Y_btnColor;
        }
        else
        {
            this.GetComponent<Renderer>().material.color = Color.white;
        }



        // Axis ----------------------------------------------------------
        float x = Input.GetAxis("X axis");
        float y = Input.GetAxis("Y axis");
        
        float x1 = Input.GetAxis("6th axis");
        float y1 = Input.GetAxis("7th axis");

        this.transform.position = center + new Vector3(x1+x, 0, y1-y);
        

    }
}
